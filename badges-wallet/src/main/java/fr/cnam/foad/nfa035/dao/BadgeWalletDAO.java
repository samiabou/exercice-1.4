package fr.cnam.foad.nfa035.dao;

import fr.cnam.foad.nfa035.dao.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.dao.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.dao.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.dao.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.dao.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.io.IOException;
import java.io.OutputStream;


public class BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);
    private File walletData;
    private ImageFileFrame media;

    /**
     * Constructeur
     * @param dataPath
     * @throws IOException
     */
    public BadgeWalletDAO(String dataPath) throws IOException{
        this.walletData = new File(dataPath);
        this.media = new ImageFileFrame(walletData);
    }

    /**
     * Méthode permet d'ajouter le badge au wallet
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image,media);
    }

    /**
     * Méthode pour récupérer le badge du wallet
     *
     * @param fileBadgeStream
     */

    public void getBadge(OutputStream fileBadgeStream) throws IOException {
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(fileBadgeStream);
        deserializer.deserialize(media);
    }
}
